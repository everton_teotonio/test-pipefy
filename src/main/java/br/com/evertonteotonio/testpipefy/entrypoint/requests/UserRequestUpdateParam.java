package br.com.evertonteotonio.testpipefy.entrypoint.requests;

import br.com.evertonteotonio.testpipefy.usecase.validator.annotations.UniqueEmail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Creatd by evert on 24 nov 2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestUpdateParam {


    private Long id;

    @NotBlank(message = "The field firstName may not be blank")
    private String firstName;

    @NotBlank(message = "The field lastName may not be blank")
    private String lastName;

    @NotBlank(message = "The field email may not be blank")
    private String email;

    public UserRequestUpdateParam(
            @NotBlank(message = "The field firstName may not be blank") String firstName,
            @NotBlank(message = "The field lastName may not be blank") String lastName,
            @NotBlank(message = "The field email may not be blank") String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

}
