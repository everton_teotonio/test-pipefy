package br.com.evertonteotonio.testpipefy.entrypoint.mapper;

import br.com.evertonteotonio.testpipefy.entrypoint.dto.UserDto;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Creatd by evert on 22 nov 2020
 */
@Mapper
public interface IResponseMapper {

    static IResponseMapper getInstance() {
        return Mappers.getMapper(IResponseMapper.class);
    }

    UserDto userDomainToDto(UserDomain userDomain);

}
