package br.com.evertonteotonio.testpipefy.entrypoint.controller;

import br.com.evertonteotonio.testpipefy.entrypoint.dto.UserDto;
import br.com.evertonteotonio.testpipefy.entrypoint.mapper.IResponseMapper;
import br.com.evertonteotonio.testpipefy.entrypoint.requests.UserRequestParam;
import br.com.evertonteotonio.testpipefy.entrypoint.requests.UserRequestUpdateParam;
import br.com.evertonteotonio.testpipefy.usecase.UserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Creatd by evert on 21 nov 2020
 */
@RestController
@RequestMapping("user")
public class UserController {

    private final UserUseCase userUseCase;

    @Autowired
    public UserController(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @GetMapping("/{email}")
    public ResponseEntity<UserDto> getUserBy(@PathVariable(value = "email") String email) {
        UserDto userDto = IResponseMapper.getInstance().userDomainToDto(userUseCase.getUserBy(email));

        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        }

        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ResponseEntity<UserDto> saveUser(@Valid @RequestBody UserRequestParam userRequestParam) {

        UserDto userDto = IResponseMapper.getInstance().userDomainToDto(userUseCase.saveUser(userRequestParam));
        return new ResponseEntity<>(
                userDto,
                HttpStatus.CREATED);
    }

    @PatchMapping
    public ResponseEntity<Object> updateUser(@Valid @RequestBody UserRequestUpdateParam userRequestParam) {
        UserDto userDto = IResponseMapper.getInstance().userDomainToDto(userUseCase.saveUpdate(userRequestParam));

        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        }

        return ResponseEntity.noContent().build();

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value = "id") Long id) {
        boolean deleted = userUseCase.deteteUserBy(id);

        if (deleted)
            return ResponseEntity.ok().build();

        return ResponseEntity.noContent().build();
    }

}
