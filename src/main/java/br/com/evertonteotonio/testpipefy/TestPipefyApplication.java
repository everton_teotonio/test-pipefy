package br.com.evertonteotonio.testpipefy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPipefyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPipefyApplication.class, args);
	}

}
