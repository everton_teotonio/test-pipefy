package br.com.evertonteotonio.testpipefy.usecase;

import br.com.evertonteotonio.testpipefy.entrypoint.requests.UserRequestParam;
import br.com.evertonteotonio.testpipefy.entrypoint.requests.UserRequestUpdateParam;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import br.com.evertonteotonio.testpipefy.usecase.gateway.UserGateway;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Creatd by evert on 21 nov 2020
 */
@Component
@AllArgsConstructor
public class UserUseCase {

    @Autowired
    private final UserGateway userGateway;

    public UserDomain getUserBy(String email) {
        return userGateway.executeFindByEmail(email);
    }

    public UserDomain saveUser(UserRequestParam userRequestParam) {
        return userGateway.saveUser(
                userRequestParam.getFirstName(),
                userRequestParam.getLastName(),
                userRequestParam.getEmail());
    }

    public UserDomain saveUpdate(UserRequestUpdateParam userRequestParam) {
        return userGateway.saveUpdate(
                userRequestParam.getId(),
                userRequestParam.getFirstName(),
                userRequestParam.getLastName(),
                userRequestParam.getEmail());
    }

    public boolean deteteUserBy(Long id){
        return userGateway.deteteUserBy(id);
    }

}
