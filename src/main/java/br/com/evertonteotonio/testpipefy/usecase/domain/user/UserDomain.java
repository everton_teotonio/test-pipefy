package br.com.evertonteotonio.testpipefy.usecase.domain.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Creatd by evert on 21 nov 2020
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDomain {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;

}
