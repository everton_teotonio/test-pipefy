package br.com.evertonteotonio.testpipefy.usecase.gateway;

import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;

/**
 * Creatd by evert on 21 nov 2020
 */
public interface UserGateway {

    UserDomain executeFindByEmail(String cpf);

    UserDomain saveUser(String firstName, String lastName, String email);

    UserDomain saveUpdate(Long id, String firstName, String lastName, String email);

    boolean deteteUserBy(Long id);

}
