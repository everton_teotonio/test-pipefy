package br.com.evertonteotonio.testpipefy.usecase.validator;

import br.com.evertonteotonio.testpipefy.usecase.UserUseCase;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import br.com.evertonteotonio.testpipefy.usecase.validator.annotations.UniqueEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Creatd by evert on 24 nov 2020
 */
@Component
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    @Autowired
    private UserUseCase userUseCase;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        UserDomain user = userUseCase.getUserBy(value);

        if (user == null) {
            return true;
        }
        return false;
    }
}
