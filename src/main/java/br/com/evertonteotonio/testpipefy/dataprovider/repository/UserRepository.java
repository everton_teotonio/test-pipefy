package br.com.evertonteotonio.testpipefy.dataprovider.repository;

import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Creatd by evert on 21 nov 2020
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmail(String email);

    Iterable<UserEntity> findByFirstName(String firstName);
}
