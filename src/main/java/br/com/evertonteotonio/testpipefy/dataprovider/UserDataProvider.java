package br.com.evertonteotonio.testpipefy.dataprovider;

import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import br.com.evertonteotonio.testpipefy.dataprovider.mapper.IEntityMapper;
import br.com.evertonteotonio.testpipefy.dataprovider.repository.UserRepository;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import br.com.evertonteotonio.testpipefy.usecase.gateway.UserGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Creatd by evert on 21 nov 2020
 */
@Component
public class UserDataProvider implements UserGateway {

    private UserRepository userRepository;

    public UserDataProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDomain executeFindByEmail(String email) {
        return IEntityMapper.getInstance().userEntityToDomain(userRepository.findByEmail(email));
    }

    @Override
    public UserDomain saveUser(String firstName, String lastName, String email) {
        return IEntityMapper.getInstance().userEntityToDomain(
                userRepository.save(new UserEntity(firstName, lastName, email)));
    }

    @Override
    public UserDomain saveUpdate(Long id, String firstName, String lastName, String email) {

        Optional<UserEntity> optionalUserEntity = userRepository.findById(id);


        if (optionalUserEntity.isPresent()) {
            UserEntity userEntity = optionalUserEntity.get();

            userEntity.setFirstName(firstName);
            userEntity.setLastName(lastName);
            userEntity.setEmail(email);
            return IEntityMapper.getInstance().userEntityToDomain(userRepository.save(userEntity));
        }

        return null;
    }

    @Override
    public boolean deteteUserBy(Long id) {

        Optional<UserEntity> optionalUserEntity = userRepository.findById(id);

        if (optionalUserEntity.isPresent()) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

}
