package br.com.evertonteotonio.testpipefy.dataprovider.mapper;

import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Creatd by evert on 21 nov 2020
 */
@Mapper
public interface IEntityMapper {

    static IEntityMapper getInstance() {
        return Mappers.getMapper(IEntityMapper.class);
    }

    UserDomain userEntityToDomain(UserEntity userEntity);

}
