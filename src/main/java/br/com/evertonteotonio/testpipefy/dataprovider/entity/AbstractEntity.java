package br.com.evertonteotonio.testpipefy.dataprovider.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Creatd by evert on 21 nov 2020
 */
@Data
@MappedSuperclass
//@SuppressWarnings("unused")
public class AbstractEntity implements Serializable {

    @CreationTimestamp
    @Column(nullable = false)
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(nullable = false)
    private Timestamp updatedAt;

}
