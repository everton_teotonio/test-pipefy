package br.com.evertonteotonio.testpipefy.dataprovider.repository;

import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Creatd by evert on 21 nov 2020
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
class UserEntityRepositoryTest {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private UserRepository userRepository;

    @Test
    void injectedComponentsAreNotNull() {
        assertThat(dataSource).isNotNull();
        assertThat(jdbcTemplate).isNotNull();
        assertThat(entityManager).isNotNull();
        assertThat(userRepository).isNotNull();
    }

    @Test
    void should_find_no_users_if_repository_is_empty() {
        Iterable<UserEntity> users = userRepository.findAll();
        assertThat(users).isEmpty();
    }

    @Test
    public void should_store_a_user() {

        String firstName = "Everton";
        String lastName = "Teotonio";
        String email = "evertintt@gmail.com";

        UserEntity user = userRepository.save(new UserEntity(firstName, lastName, email));

        assertThat(user).hasFieldOrPropertyWithValue("firstName", firstName);
        assertThat(user).hasFieldOrPropertyWithValue("lastName", lastName);
        assertThat(user).hasFieldOrPropertyWithValue("email", email);
    }

    @Test
    public void should_find_all_users() {
        UserEntity user1 = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user1);

        UserEntity user2 = new UserEntity("Everton", "Teotonio", "evertontt1@gmail.com");
        entityManager.persist(user2);

        UserEntity user3 = new UserEntity("Everton", "Teotonio", "evertontt2@gmail.com");
        entityManager.persist(user3);

        Iterable<UserEntity> tutorials = userRepository.findAll();

        assertThat(tutorials).hasSize(3).contains(user1, user2, user3);
    }

    @Test
    public void should_find_user_by_id() {
        UserEntity user1 = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user1);

        UserEntity user2 = new UserEntity("Everton", "Teotonio", "evertontt1@gmail.com");
        entityManager.persist(user2);

        UserEntity foundUser = userRepository.findById(user2.getId()).get();

        assertThat(foundUser).isEqualTo(user2);
    }

    @Test
    public void should_find_published_users() {
        UserEntity user1 = new UserEntity("Everton", "Teotonio", "evertontt2@gmail.com");
        entityManager.persist(user1);

        UserEntity user2 = new UserEntity("Everton1", "Teotonio", "evertontt1@gmail.com");
        entityManager.persist(user2);

        UserEntity user3 = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user3);

        Iterable<UserEntity> users = userRepository.findByFirstName("Everton");

        assertThat(users).hasSize(2).contains(user1, user3);
    }

    @Test
    public void should_update_user_by_id() {
        UserEntity user1 = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user1);

        UserEntity user2 = new UserEntity("Everton 2", "Teotonio 2", "evertontt2@gmail.com");
        entityManager.persist(user2);

        UserEntity updatedTut = new UserEntity("Everton 22", "Teotonio 22", "evertontt22@gmail.com");

        UserEntity tut = userRepository.findById(user2.getId()).get();
        tut.setEmail(updatedTut.getEmail());
        tut.setFirstName(updatedTut.getFirstName());
        tut.setLastName(updatedTut.getLastName());
        userRepository.save(tut);

        UserEntity checkTut = userRepository.findById(user2.getId()).get();

        assertThat(checkTut.getId()).isEqualTo(user2.getId());
        assertThat(checkTut.getEmail()).isEqualTo(updatedTut.getEmail());
        assertThat(checkTut.getFirstName()).isEqualTo(updatedTut.getFirstName());
        assertThat(checkTut.getEmail()).isEqualTo(updatedTut.getEmail());
    }

    @Test
    public void should_delete_user_by_id() {
        UserEntity user1 = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user1);

        UserEntity user2 = new UserEntity("Everton", "Teotonio", "evertontt1@gmail.com");
        entityManager.persist(user2);

        UserEntity user3 = new UserEntity("Everton", "Teotonio", "evertontt2@gmail.com");
        entityManager.persist(user3);

        userRepository.deleteById(user2.getId());

        Iterable<UserEntity> tutorials = userRepository.findAll();

        assertThat(tutorials).hasSize(2).contains(user1, user3);
    }

    @Test
    public void should_delete_all_users() {
        entityManager.persist(new UserEntity("Everton", "Teotonio", "evertontt@gmail.com"));
        entityManager.persist(new UserEntity("Everton", "Teotonio", "evertontt1@gmail.com"));

        userRepository.deleteAll();

        assertThat(userRepository.findAll()).isEmpty();
    }

    @Test
    public void should_return_an_exception_if_add_duplicate_email() {

        entityManager.persist(new UserEntity("Everton", "Teotonio", "evertontt@gmail.com"));

        PersistenceException ex = assertThrows(
                PersistenceException.class,
                () -> entityManager.persist(new UserEntity("Everton", "Teotonio", "evertontt@gmail.com")));

        assertEquals(ConstraintViolationException.class, ex.getCause().getClass());

    }

}
