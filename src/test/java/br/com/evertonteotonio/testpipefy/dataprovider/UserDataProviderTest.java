package br.com.evertonteotonio.testpipefy.dataprovider;

import br.com.evertonteotonio.testpipefy.TestPipefyApplication;
import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import br.com.evertonteotonio.testpipefy.dataprovider.repository.UserRepository;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Creatd by evert on 21 nov 2020
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = {TestPipefyApplication.class, UserDataProvider.class})
class UserDataProviderTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDataProvider userDataProvider;

    @Test
    void should_get_user_by_email() {

        UserEntity user = userRepository.save(new UserEntity("Everton", "Teotonio", "evertontt@gmail.com"));

        UserDomain userDomain = userDataProvider.executeFindByEmail("evertontt@gmail.com");
        assertThat(user.getEmail()).isEqualTo(userDomain.getEmail());
        assertThat(user.getFirstName()).isEqualTo(userDomain.getFirstName());
        assertThat(user.getLastName()).isEqualTo(userDomain.getLastName());

    }
}
