package br.com.evertonteotonio.testpipefy.usecase;

import br.com.evertonteotonio.testpipefy.TestPipefyApplication;
import br.com.evertonteotonio.testpipefy.dataprovider.UserDataProvider;
import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import br.com.evertonteotonio.testpipefy.usecase.domain.user.UserDomain;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Creatd by evert on 22 nov 2020
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
@ContextConfiguration(classes = {TestPipefyApplication.class, UserUseCase.class, UserDataProvider.class})
class UserUseCaseTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private UserUseCase userUseCase;


    @Test
    void should_get_user_by_email() {

        UserEntity user = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user);

        UserDomain userDomain = userUseCase.getUserBy("evertontt@gmail.com");

        assertThat(user.getEmail()).isEqualTo(userDomain.getEmail());
        assertThat(user.getFirstName()).isEqualTo(userDomain.getFirstName());
        assertThat(user.getLastName()).isEqualTo(userDomain.getLastName());

    }

    @Test
    void should_not_get_user_by_email() {

        UserEntity user = new UserEntity("Everton", "Teotonio", "evertontt@gmail.com");
        entityManager.persist(user);

        UserDomain userDomain = userUseCase.getUserBy("evertontt1@gmail.com");
        assertThat(userDomain).isNull();

    }
}
