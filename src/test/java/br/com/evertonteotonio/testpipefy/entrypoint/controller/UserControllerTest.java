package br.com.evertonteotonio.testpipefy.entrypoint.controller;

import br.com.evertonteotonio.testpipefy.dataprovider.entity.UserEntity;
import br.com.evertonteotonio.testpipefy.dataprovider.repository.UserRepository;
import br.com.evertonteotonio.testpipefy.entrypoint.dto.UserDto;
import br.com.evertonteotonio.testpipefy.entrypoint.requests.UserRequestParam;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


/**
 * Creatd by evert on 22 nov 2020
 */

@ExtendWith(SpringExtension.class)
@AutoConfigureJsonTesters
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class UserControllerTest {

    @Value("${local.server.port}")
    int port;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .build();
    }


    @Test
    void whenValidInput_thenReturns200() throws Exception {

        this.userRepository.save(new UserEntity("Everton", "Teotonio", "evertontt456@gmail.com"));

        this.mockMvc.perform(
                get("/user/{email}", "evertontt@gmail.com")
                        .contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    void whenValidInput_thenReturns204() throws Exception {

        this.userRepository.save(new UserEntity("Everton", "Teotonio", "evertonteotoniott@gmail.com"));

        this.mockMvc.perform(
                get("/user/{email}", "teotonio@gmail.com")
                        .contentType("application/json"))
                .andExpect(status().isNoContent());

    }

    @Test
    void saveUser_whenValidInput_thenReturns201() throws Exception {

        UserRequestParam user = new UserRequestParam("Everton", "Teotonio", "evertontt12@gmail.com");
        ResponseEntity<UserDto> userRequestParamResponseEntity = restTemplate.postForEntity("/user/",
                user, UserDto.class);

        assertThat(userRequestParamResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    }

    @Test
    void updateUser_saveUser_whenValidInput_thenReturns200() {

        UserEntity userEntity = this.userRepository.save(
                new UserEntity("Everton", "Teotonio", "evertontt@gmail.com"));

        UserRequestParam user = new UserRequestParam(
                userEntity.getId(),
                "Everton",
                "Teotonio atualizado",
                "evertontt@gmail.com");


        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "merge-patch+json");
        headers.setContentType(mediaType);

        HttpEntity<UserRequestParam> entity = new HttpEntity<>(user, headers);
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        ResponseEntity<UserDto> responseEntity = restTemplate.exchange(
                "http://localhost:" + this.port + "/user", HttpMethod.PATCH, entity, UserDto.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);


    }

    @Test
    void deleteUser_thenReturns200() {

        UserEntity userEntity = this.userRepository.save(
                new UserEntity("Everton", "Teotonio", "evertontt8375648@gmail.com"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<UserRequestParam> entity = new HttpEntity<>(null, headers);
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        ResponseEntity<UserDto> responseEntity = restTemplate.exchange(
                "http://localhost:" + this.port + "/user/" + userEntity.getId(), HttpMethod.DELETE, entity, UserDto.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

    }
}
