package br.com.evertonteotonio.testpipefy.entrypoint.controller;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Creatd by evert on 22 nov 2020
 */
@SpringBootTest
public class CustomerModuleWebLayerTests {

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .build();
    }

    @Autowired
    private UserController userController;

    @Test
    void onlyCustomerControllerIsLoaded() {
        assertThat(userController).isNotNull();
    }
}
