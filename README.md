**Project based in Spring [Boot version 2.4.0](https://spring.io/projects/spring-boot) and [clean architecture](https://www.planetgeek.ch/wp-content/uploads/2016/03/Clean-Architecture-V1.0.pdf)**

# Pre requirements

- [Maven >= 3.6.3](https://maven.apache.org/download.cgi)
- [Java = 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)


# Run app

- mvn spring-boot:run

# Run all tests app

- mvn test

# Copyright

- Released under the Apache License 2.0. [See the LICENSE file](https://www.apache.org/licenses/LICENSE-2.0.txt).